CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
The Autorevision module helps content editors to track changes on nodes by auto enabling revision on preselected content types.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/epavlov/2476843


 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2476843

REQUIREMENTS
------------
This module has no requirements.

RECOMMENDED MODULES
-------------------
 * Node Revision Delete (https://www.drupal.org/project/node_revision_delete):
   The Node Revision Delete module lets you to track and prune old revisions of content types.
   Since Autorevision can create a lot of revisions with time, Node Revision Delete module can help eliminate them and clean-up database.
   
INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * To configure Autorevision in Administration » Configuration » Autorevision:

   - Select for which content types new revision will be created automatically and save configuration

MAINTAINERS
-----------
Current maintainers:
 * Eugene Pavlov (epavlov) - https://www.drupal.org/u/epavlov
 * Vova Kenidra (kedramon) - https://www.drupal.org/u/kedramon